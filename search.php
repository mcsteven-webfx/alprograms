<?php
/**
*
*  Search Page
*
*  @link https://codex.wordpress.org/Template_Hierarchy
*  @package Alprograms
*  @since 2019
*
**/
?>

<?php get_header(); ?>

  <?php $bg = "url('".get_template_directory_uri()."/assets/img/category/category1.png') no-repeat fixed center; background-size: cover;"?>
  <div class="categoryContainer" style="background: <?php echo $bg; ?>;">
    <div class="container--section">

      <div class="category--background">
        <div class="categoryTitle">
          <p class="category__title">Search Results for : <?php the_search_query(); ?></p>
        </div>
      </div>

    </div>
  </div>

  <?php get_search_form(); ?>
  <div class="categoryContent">
    <div class="container--section">
      <div class="categoryContent--wrapper">
        <?php
        if(have_posts()):
          $types = array(
            array('type' => '1094', ),
            array('type' => '8', )
          );

          foreach($types as $type):
          $post_type = array(
            'post_type' => 'post',
            'cat' => $type['type'],
            'post_status' => 'publish',
            'posts_per_page'=>-1,
            'order' => 'asc',
            'orderby' => 'post-date',
            's' => get_search_query() );

          $course_query = new WP_query($post_type);

          if ( $course_query->have_posts() ) :
            while ( $course_query->have_posts() ) : $course_query->the_post();
        ?>
              <a href="<?php the_permalink(); ?>">

                <?php
                  $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                  $bg = 'background: url('.$url.') no-repeat center; background-size: cover;';
                  $days = get_post_meta(get_the_ID(), 'course_days', true);
                  $level = get_post_meta(get_the_ID(), 'course_level', true);
                  $lectures = get_post_meta(get_the_ID(), 'course_lectures', true);
                ?>

                <div class="categoryContent--box">
                  <div class="categoryContent--image" style="<?php echo $bg?>"></div>
                  <div class="categoryContent--text">
                    <p class="categoryContent__title"><?php the_title(); ?></p>
                  </div>
                  <div class="categoryContent--descriptions">
                    <?php if(!empty($lectures)): ?>
                      <span class="categoryContent__lectures"><?php echo ($lectures .' Lecture'. ($lectures > 1 ? 's' : '')); ?></span>
                    <?php endif; ?>
                    <?php if(!empty($days)): ?>
                      <span class="categoryContent__session"><?php echo ($days .' Session'. ($days > 1 ? 's' : '')); ?></span>
                    <?php endif; ?>
                    <?php if(!empty($level)): ?>
                      <span class="categoryContent__level"><?php echo $level; ?> Level</span>
                    <?php endif; ?>
                  </div>
                </div>
              </a>
          <?php
            endwhile;
          endif;
          endforeach;
        endif;
      ?>
      </div>
    </div>
  </div>
<?php get_footer(); ?>
