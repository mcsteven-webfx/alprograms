jQuery(document).ready(function( $ ) {
  var scroll_pos = 0;
  $(document).scroll(function() {
    scroll_pos = $(this).scrollTop();
    if(scroll_pos > 50) {
        $("header").css('background-color', '#0C6D63');
    } else {
        $("header").css('background-color', 'transparent');
    }
  });

  $(".mobile--nav").click(function() {
    $(".header--navigation").slideToggle();
  });

  $("#inHouseCard").click(function() {
    $(".inhouse--card").show();
    $(".public--card").hide();
  });

  $("#publicCard").click(function() {
    $(".inhouse--card").hide();
    $(".public--card").show();
  });
});
