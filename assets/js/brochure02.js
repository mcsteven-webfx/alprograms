jQuery(document).ready(function( $ ) {
  $('.downloadpdf-button').click(function() {
    var link = $(this).attr("data-link");

    if(link.length > 0) {
      window.open(link, '_blank');
    }
  });
});
