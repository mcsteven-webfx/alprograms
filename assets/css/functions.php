<?php
/* ===================
    AlP's Function's
   ===================
*/


add_action('init', 'start_session', 1);
add_action('wp_logout', 'end_session');
add_action('wp_login', 'end_session');

function start_session() {
    if(!session_id()) {
        session_start();
    }
}

function end_session() {
    session_destroy ();
}

add_theme_support( 'title-tag' );

function ALPrograms_setup(){
  //Register Primary menu
  register_nav_menu( 'primary', 'First Header Navigation');
  //Register Footer menu
  register_nav_menu( 'secondary', 'Secondary Footer Navigation');
  //Enable support menu.
  add_theme_support( 'menus' );
  //Enable support for Post Thumbnails on posts and pages.
  add_theme_support( 'post-thumbnails' );
  //Enable Except in Post
  add_post_type_support( 'post','excerpt');
  //Enable Except in Page
  add_post_type_support( 'page', 'excerpt' );
}
add_action('init','ALPrograms_setup');

function ALPrograms_CSS_Resources(){
  wp_enqueue_style( 'Alprograms', get_stylesheet_uri() );
  //Header and Footer
  wp_enqueue_style( 'Header-Footer', get_template_directory_uri() .'/css/global.css');
  //Home Page
  wp_enqueue_style( 'Home-Page', get_template_directory_uri() .'/css/home15.css');
  //In House Page
  wp_enqueue_style( 'In-House-Page', get_template_directory_uri() .'/css/category6.css');
  //Single Post Page
  wp_enqueue_style( 'Single-Page', get_template_directory_uri() .'/css/single12.css');
  //Quotation Page
  wp_enqueue_style( 'Quotation-Page', get_template_directory_uri() .'/css/quotation1.css');
  //DownloadPDF Page
  wp_enqueue_style( 'DownloadPDF-Page', get_template_directory_uri() .'/css/downloadPDF6.css');
  //Search Page
  wp_enqueue_style( 'Search-Page', get_template_directory_uri() .'/css/search6.css');
  //Terms and Privacy Page
  wp_enqueue_style( 'TermsAndPrivacy-Page', get_template_directory_uri() .'/css/termandprivacy1.css');
  //About Us Page
  wp_enqueue_style( 'AboutUs-Page', get_template_directory_uri() .'/css/about1.css');
  //List of Clients Us Page
  wp_enqueue_style( 'ListOfClients-Page', get_template_directory_uri() .'/css/listofclients10.css');
  //Google Icons
  wp_enqueue_style( 'Google-icons', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
  //Animate.CSS
  wp_enqueue_style( 'CSS Animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css');
  //Swiper CSS
  wp_enqueue_style( 'Swiper CSS', 'https://unpkg.com/swiper/css/swiper.css');
  //Swiper MIN
  wp_enqueue_style( 'Swiper Min', 'https://unpkg.com/swiper/css/swiper.min.css');
  //Swiper JS
  wp_enqueue_script( 'Swiper Js', 'https://unpkg.com/swiper/js/swiper.js"');
  //Swiper Js Min
  wp_enqueue_script( 'Swiper JS Min', 'https://unpkg.com/swiper/js/swiper.min.js');
  //bootstrap
  wp_enqueue_script( 'alps-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', '3.3.7', true);
  //recaptcha
  wp_enqueue_script( 'alps-recaptcha', 'https://www.google.com/recaptcha/api.js');
  //Swiper
  wp_enqueue_script('swiper', get_template_directory_uri() . '/js/swiper.js', array('jquery'), '1.0.0', true);
  //Fade In
  wp_enqueue_script('fade-in', get_template_directory_uri() . '/js/onscroll.js', array('jquery'), '1.0.0', true);
}
add_action( 'wp_enqueue_scripts', 'ALPrograms_CSS_Resources');

/**
* Custom Fields Array
* - Sessions
* - Lectures
* - Level
**/
$prefix = 'course_';
$custom_meta_fields = array(
	array(
	    'label' => 'Sessions/Days',
	    'desc'  => 'Number of days spinner',
	    'id'    => $prefix.'days',
	    'type'  => 'number',
	    'min'   => '0',
	),
	array(
		'label' => 'Lectures',
		'desc'  => 'Number of lectures spinner',
		'id'    => $prefix.'lectures',
		'type'  => 'number',
		'min'   => '0',
	),
	array(
		'label' => 'Level',
		'desc'	=> 'Course difficulty',
		'id'	=> $prefix.'level',
		'type'	=> 'radio',
		'options' => array (

			'Beginner' => array (
				'label' => 'Beginner',
				'value' => 'Beginner'
			),
			'Intermediate' => array (
				'label' => 'Intermediate',
				'value' => 'Intermediate'
			),
			'Advance' => array (
				'label' => 'Advance',
				'value' => 'Advance'
			),
			'Expert' => array (
				'label' => 'Expert',
				'value' => 'Expert'
			)
		)
	),
  array(
	    'label' => 'Price',
	    'desc'  => 'Price of the course',
	    'id'    => $prefix.'price',
	    'type'  => 'number',
	    'min'   => '0',
	),
  array(
      'label' => 'Max',
      'desc'  => 'maximum people per course',
      'id'    => $prefix.'max',
      'type'  => 'number',
      'min'   => '0',
  ),
  array(
      'label' => 'Per Head',
      'desc'  => 'Per Head price',
      'id'    => $prefix.'head',
      'type'  => 'number',
      'min'   => '0',
  )
);

/*
   =================
   REGISTER CUSTOM META BOXES
   ==================
*/
function register_custom_meta_box(){
  //OUTLINE EDITOR
  add_meta_box( 'post_outline', __('Outline', 'wysiwyg'), 'course_outline', 'post');
  //MODULE EDITOR
  add_meta_box( 'post_module', __('Module', 'wysiwyg'), 'course_module', 'post');
  //CUSTOM FIELD
  add_meta_box( 'post', 'custom_fields', 'show_course_custom_fields', 'post');
}add_action( 'admin_init', 'register_custom_meta_box');

/*
   =================
   FUNCTION FOR OUTLINE EDITOR
   ==================
*/
function course_outline($post) {
	$content = get_post_meta($post->ID, 'course_outline_wysiwyg', true);
	wp_editor(htmlspecialchars_decode($content) , 'course_outline_wysiwyg', array("media_buttons" => true));
}

/*
   =================
   FUNCTION FOR MODULE EDITOR
   ==================
*/
function course_module($post) {
	$content = get_post_meta($post->ID, 'course_module_wysiwyg', true);
	wp_editor(htmlspecialchars_decode($content) , 'course_module_wysiwyg', array("media_buttons" => true));
}

/*
   =================
   SAVE DATA FOR OUTLINE EDITOR AND MODULE EDITOR
   ==================
*/
function custom_wysiwyg_save_postdata($post_id) {
	global $custom_meta_fields;

	// verify nonce
	if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}

  /*
     =================
     EDITOR IN OUTLINE AND MODULE
     ==================
  */
	$data = $_POST['course_outline_wysiwyg'];
	update_post_meta($post_id, 'course_outline_wysiwyg', $data);
	$data = $_POST['course_module_wysiwyg'];
	update_post_meta($post_id, 'course_module_wysiwyg', $data);

 }
add_action('save_post', 'custom_wysiwyg_save_postdata');

/*
   =================
   CALLBACK
   ==================
*/
function show_course_custom_fields() {
	global $custom_meta_fields, $post;
	// Use nonce for verification
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	    // Begin the field table and loop
	    echo '<table class="form-table">';
	    foreach ($custom_meta_fields as $field) {
	        // get value of this field if it exists for this post
	        $meta = get_post_meta($post->ID, $field['id'], true);
	        // begin a table row with
	        echo '<tr>
	                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
	                <td>';
	                switch($field['type']) {
						case 'number':
							$value = $meta != '' ? $meta : $field['min'];
							    echo '<div id="'.$field['id'].'"></div>
							            <input type="'.$field['type'].'" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" min="'.$field['min'].'"/>
							            <br /><span class="description">'.$field['desc'].'</span>';
							break;
						case 'radio':
							foreach( $field['options'] as $option ) :
								echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'"'. ($meta == $option['value'] ? ' checked="checked"' : '') .'/>
									  <label for="'.$option['value'].'">'.$option['label'].'</label><br/>';
							endforeach;
							break;
            /*case 'text':
                echo '<div id="'.$field['id'].'"></div>
                        <input type="'.$field['type'].'" name="'.$field['id'].'" id="'.$field['id'].'" autocomplete="off"/>
                        <br /><span class="description">'.$field['desc'].'</span>';
              break;*/
	                } //end switch
	        echo '</td></tr>';
	    } // end foreach
	    echo '</table>'; // end table
}

/*
   =================
   SAVE THE DATA
   ==================
*/

function save_custom_meta_data($post_id) {
	// loop through fields and save the data
  global $custom_meta_fields;

	foreach ($custom_meta_fields as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif (' ' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // end foreach
}
add_action( 'save_post', 'save_custom_meta_data');

/*
   =================
   ADD CUSTOM COLUMNS
   ==================
*/

function add_post_columns($columns){
  global $custom_meta_fields;
	foreach($custom_meta_fields as $field) {
		$columns[$field['id']] = __($field['label'], 'ALPrograms');
	}
    return $columns;
}
add_filter( 'manage_post_posts_columns', 'add_post_columns');

/*
   =================
   POST THE VALUE IN THE CUSTOM COLUMNS
   ==================
*/

function custom_columns( $column, $post_id ) {
	echo get_post_meta($post_id, $column, true);
}
add_action('manage_post_posts_custom_column' , 'custom_columns', 10, 2 );

/*
  =================
  MAKE COLUMNS SORTABLE
  ==================
*/

function add_post_columns_sortable($columns) {
	global $custom_meta_fields;
	foreach($custom_meta_fields as $field) {
		$columns[$field['id']] = $field['id'];
	}
  $columns['categories'] = 'categories';
  return $columns;
}
add_filter('manage_edit-post_sortable_columns' , 'add_post_columns_sortable');

/*
  =================
    ORDER THE CUSTOM COLUMNS
    ==================
  */

function manage_wp_posts_pre_get_posts( $query ) {

	$orderby = $query->get('orderby');

	if($orderby == 'course_lectures' || $orderby == 'course_days') {
		$query->set( 'meta_key', $orderby);
		$query->set( 'orderby', 'meta_value_num' );
	} else if($orderby == 'course_level' || $orderby == 'course_featured') {
		$query->set( 'meta_key', $orderby);
		$query->set( 'orderby', 'meta_value' );
	}

}
add_action( 'pre_get_posts', 'manage_wp_posts_pre_get_posts');

/*
  =================
  SEARCH BY TITLE ONLY
  ==================
*/

function search_by_title_only( $search, $wp_query )
{
    global $wpdb;
    if(empty($search)) {
        return $search; // skip processing - no search term in query
    }
    $q = $wp_query->query_vars;
    $n = !empty($q['exact']) ? '' : '%';
    $search =
    $searchand = '';
    foreach ((array)$q['search_terms'] as $term) {
        $term = esc_sql($wpdb->esc_like($term));
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }
    if (!empty($search)) {
        $search = " AND ({$search}) ";
        if (!is_user_logged_in())
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }
    return $search;
}
add_filter('posts_search', 'search_by_title_only', 500, 2);

function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

/*
  =================
  ADD CUSTOM TAXONOMIES
  ==================
*/

function add_custom_types_to_tax( $query ) {
	if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

		// Get all your post types
		$post_types = get_post_types();

		$query->set( 'post_type', $post_types );
		return $query;
	}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );

/*
  =================
  ADD CATEGORY
  ==================
*/
function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
	register_taxonomy_for_object_type( 'category', 'page' );
}
add_action( 'init', 'add_taxonomies_to_pages' );
