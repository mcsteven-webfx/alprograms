<?php

/**
*
*  Page qoutation PDF
*  @package Alprograms
*  @since 2019
*
**/

session_start();

require(dirname(__FILE__).'/mpdf/mpdf.php');

class CoursePDF extends mPDF {
  var $nav_logo;
  var $company_name;
  var $phone;
  var $email;

  var $fullname;
  var $courses;
  var $totalvat;
  var $total;
  var $nop;

 function setNavLogo($nav_logo) {
      $this->nav_logo = $nav_logo;
  }
  function setCompanyName($company_name) {
      $this->company_name = $company_name;
  }
  function setPhoneNumber($phone) {
      $this->phone = $phone;
  }
  function setEmail($email) {
      $this->email = $email;
  }
  function setName($fullname) {
      $this->fullname = $fullname;
  }
  function setCourses($courses) {
      $this->courses = $courses;
  }
  function setTotalVat($totalvat) {
      $this->totalvat = $totalvat;
  }
  function setTotal($total) {
      $this->total = $total;
  }
  function setNOP($nop) {
      $this->nop = $nop;
  }

  function showPage() {
      if(!empty($this->nav_logo)) {
          $this->Image("https://www.alprograms.com/wp-content/uploads/2017/05/logo-01.png",15,10,60,-400);
      }
      $this->WriteHTML('<h4 style="margin-top: -80px; text-align: right; font-family: Arial; margin-bottom: 0px;">'.$this->company_name.'</h4>');
      $this->WriteHTML('<p style="margin: 0px; text-align: right; font-family: Arial; font-size: 12px;">'.$this->email.'</p>');
      $this->WriteHTML('<p style="margin: 0px; text-align: right; font-family: Arial; font-size: 12px;">Phone: '.$this->phone.'</p>');

      $this->Ln(10);
      $this->WriteHTML('Hi, '.$this->fullname);
      $this->Ln(2);
      $this->WriteHTML('<p style="margin-left:40px;">'.'I hope you are doing well.'.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'Thank you for reaching out today and letting us know about your requirements for '.$this->courses.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'I would like to send you my proposal virtual-instructor-led program with the below inclusions:'.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'1. Trainer to deliver the course through Zoom, Google Meet or your preferred online platform  <br> 2. Training Materials in PDF <br> 3. Training delivery <br> 4. In-training exercises <br> 5. Post training action plans <br> 6. Certificate of Completion in PDF'.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'Both the course outline and course objectives are available in the course brochure that comes with this quotation. Please let me know if you did not receive it.'.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'Our courses are based on learning workshops and activities. It is encouraged to use laptops or tablets to access training materials. ALPs is an advocate of paperless training'.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'The training investment fees for this course:'.'</p>');
      $this->Ln(1);
      $this->WriteHTML('<p style="margin-left:40px;">'.'Number of Participants: '.$this->nop.'</p>');
      $this->WriteHTML('<p style="margin-left:40px;">'.'Sub-Total: '.$this->total.'</p>');
      $this->WriteHTML('<p style="margin-left:40px;">'.'Vat: 12%'.'</p>');
      $this->WriteHTML('<p style="margin-left:40px;">'.'Total: '.$this->totalvat.'</p>');
      $this->Ln(5);
      $this->WriteHTML('<p style="margin-left:40px;">'.'Please let me know if you have any questions and have a great day!'.'</p>');
      $this->Ln(10);
      $this->WriteHTML('<p style="text-align:left;">'.'Paul Mendoza'.'</p>');
      $this->WriteHTML('<p style="text-align:left; margin: 0px;">'.'Director - Training Delivery'.'</p>');
      $this->WriteHTML('<p style="text-align:left; margin: 0px;">'.'Tel:02 506 8016'.'</p>');
      $this->WriteHTML('<p style="text-align:left; margin: 0px;">'.'Mobile: 917 6247037'.'</p>');
      $this->WriteHTML('<p style="text-align:left; margin: 0px;">'.'Email: paul@alprograms.com'.'</p>');
      $this->WriteHTML('<p style="text-align:left; margin: 0px;">'.'website: www.alprograms.com'.'</p>');

  }
}

$fullname = $_SESSION['fullname'];
$courses = $_SESSION['courses'];
$totalvat = $_SESSION['totalvat'];
$total = $_SESSION['total'];
$nop = $_SESSION['nop'];


$footer = '<p style="text-align: center; font-size: 12px; font-style: italic;">Advanced Learning Programs</p>';
$footer.= '<p style="text-align: center; font-size: 12px; font-style: italic;">Request Quotation</p>';

$pdf = new CoursePDF();

$pdf->SetHTMLFooter($footer);
$pdf->setNavLogo($company_logo);
$pdf->setCompanyName("Advanced Learning Programs");
$pdf->setPhoneNumber("(02) 7-902 0992 / (02) 7-506 8016");
$pdf->setEmail("support@alprograms.com");
$pdf->AddPage();
$pdf->image("https://www.alprograms.com/wp-content/uploads/2017/05/logo-01.png",15,10,35,30);
$pdf->setName($fullname);
$pdf->setCourses($courses);
$pdf->setTotalVat($totalvat);
$pdf->setTotal($total);
$pdf->setNOP($nop);

$pdf->showPage();

$pdf->Output($title.'.pdf', 'I');
