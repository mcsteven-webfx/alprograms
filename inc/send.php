<?php
if(isset($_POST['submit']) && !empty($_POST['submit'])):
  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):

    //your site secret key
    $secret = '6Lc3YVkUAAAAAANbINejzYuOU6MfqzpEa6sbyU_H';
    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success):

      $fullname = !empty($_POST['fullname'])?$_POST['fullname']:'';
      $NoP = !empty($_POST['nop'])?$_POST['nop']:'';
      $courses = !empty($_POST['courses'])?$_POST['courses']:'';
      $mobilenumber = !empty($_POST['mobilenumber'])?$_POST['mobilenumber']:'';
      $email = !empty($_POST['email'])?$_POST['email']:'';

      $to = 'support@alprograms.com';
      $subject = 'New Contact form have been submitted';
      $htmlContent = "
          <h1>Quotation request details</h1>
          <p><b>Name: </b>".$fullname."</p>
          <p><b>Email: </b>".$email."</p>
          <p><b>Mobilenumber: </b>".$mobilenumber."</p>
          <p><b>Course: </b>".$courses."</p>
      ";
      // Always set content-type when sending HTML email
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


      //send email
      wp_mail($to,$subject,$htmlContent,$headers);

      global $post;
      $args = array( 'numberposts' => -1);
      $posts = get_posts($args);
      foreach( $posts as $post ) :
        if($courses == get_the_title()){
          $Nop1 = (int)$NoP;
          $PerH = get_post_meta(get_the_ID(), 'course_head', true);
          $Total = $Nop1 * $PerH;
          $Vat = $Total * 0.12;
          $TotalVAT = ($Total * 0.12)+$Total;
          setup_postdata($post);
        }
      endforeach;

      $_SESSION['fullname'] = $fullname;
      $_SESSION['courses'] = $courses;
      $_SESSION['location'] = $location;
      $_SESSION['totalvat'] = $TotalVAT;
      $_SESSION['total'] = $Total;
      $_SESSION['nop'] = $NoP;
      $_SESSION['display'] = 1;
      wp_redirect('https://www.alprograms.com/download-pdf');
      exit();

    else:
      $errMsg = 'Robot Verification failed, Please try again.';
    endif;
  else:
    $errMsg = 'Please click on the reCAPTCHA box.';
  endif;
endif;
get_header();

get_footer();
?>
