<?php

/**
*
*  Download Post PDF
*  @package Alprograms
*  @since 2019
*
**/

session_start();

require(dirname(__FILE__).'/mpdf/mpdf.php');

class CoursePDF extends mPDF {

    var $nav_logo;
    var $company_name;
    var $phone;
    var $email;

    var $title;
    var $description;
    var $objective;
    var $modules;

   function setNavLogo($nav_logo) {
        $this->nav_logo = $nav_logo;
    }
    function setCompanyName($company_name) {
        $this->company_name = $company_name;
    }
    function setPhoneNumber($phone) {
        $this->phone = $phone;
    }
    function setEmail($email) {
        $this->email = $email;
    }
    function setTitle($title) {
        $this->title = $title;
    }
    function setDescription($description) {
        $this->description = $description;
    }
    function setObjective($objective) {
        $this->objective = $objective;
    }
    function setModules($modules) {
        $this->modules = $modules;
    }


    function showPage() {
        if(!empty($this->nav_logo)) {
            $this->Image("https://www.alprograms.com/wp-content/uploads/2017/05/logo-01.png",15,10,60,-400);
        }
        $this->WriteHTML('<h4 style="margin-top: -80px; text-align: right; font-family: Arial; margin-bottom: 0px;">'.$this->company_name.'</h4>');
        $this->WriteHTML('<p style="margin: 0px; text-align: right; font-family: Arial; font-size: 12px;">'.$this->email.'</p>');
        $this->WriteHTML('<p style="margin: 0px; text-align: right; font-family: Arial; font-size: 12px;">Phone: '.$this->phone.'</p>');

        $this->Ln(10);

        $this->WriteHTML('<h3 style="font-family: Arial;">'.$this->title.'</h3>');
        $this->WriteHTML($this->description);

        $this->Ln(5);

        $this->WriteHTML('<h3 style="font-family: Arial;">Objective</h3>');
        $this->WriteHTML($this->objective);

        $this->Ln(5);

        $this->WriteHTML('<h3 style="font-family: Arial;">Module</h3>');
        $this->WriteHTML($this->modules);
        $this->Ln(5);
    }


}

$title = $_SESSION['course_title'];
$description = $_SESSION['course_description'];
$objective = $_SESSION['course_outline'];
$modules = $_SESSION['course_module'];

$company_name = $_SESSION['company_name'];
$company_logo = $_SESSION['company_logo'];
$company_phone = $_SESSION['company_phone'];
$company_email = $_SESSION['company_email'];

$footer = '<p style="text-align: center; font-size: 12px; font-style: italic;">'.$company_name.'</p>';
$footer.= '<p style="text-align: center; font-size: 12px; font-style: italic;">'.$title.'</p>';

$pdf = new CoursePDF();

$pdf->SetHTMLFooter($footer);
$pdf->setNavLogo($company_logo);
$pdf->setCompanyName("Advanced Learning Programs");
$pdf->setPhoneNumber("(02) 7-902 0992 / (02) 7-506 8016");
$pdf->setEmail("support@alprograms.com");
$pdf->setTitle($title);
$pdf->AddPage();
$pdf->image("https://www.alprograms.com/wp-content/uploads/2017/05/logo-01.png",20,10,35,30);
$pdf->setTitle($title);
$pdf->setDescription($description);
$pdf->setObjective($objective);
$pdf->setModules($modules);
$pdf->showPage();


$pdf->Output($title.'.pdf', 'I');
