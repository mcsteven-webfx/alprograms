<?php

/**
*
*  Footer Navigation
*  @package Alprograms
*  @since 2019
*
**/

?>

    <footer>
      <div class="container--section container--flex">
        <div class="footer__image">
          <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="logo"></a>
        </div>
        <div class="footer--content">
          <nav class="footer--navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
          </nav>
          <div class="footer__line"></div>
          <div class="footer--contacts">
            <div class="footerContacts">
              (02) 7-902 0992 / 0917 6247037 <br>
              <a href="mailto:support@alprograms.com">support@alprograms.com</a>
            </div>
            <div class="footerSocial">
              <a href="https://www.facebook.com/AdvancedLearningPrograms/" target="_blank"><i class='fab fa-facebook-square'></i></a>
              <a href="https://ph.linkedin.com/company/advancedlearningprograms" target="_blank"><i class='fab fa-linkedin'></i></a>
              <a href="https://www.instagram.com/advanced_learning_programs/?hl=en" target="_blank"><i class='fab fa-instagram'></i></a>
			  <a href="https://twitter.com/alprograms?lang=en" target="_blank"><i class='fab fa-twitter'></i></a>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <?php wp_footer(); ?>
    </div>
  </body>
</html>
