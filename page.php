<?php

/**
*
*  Default Page Template
*  @package Alprograms
*  @since 2019
*
**/

?>

<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

  <div class="pageContainer" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>') no-repeat center; background-size: cover;">
    <div class="container--section">
      <div class="page--content">
        <p class="page__title">
          <?php echo get_the_title(); ?>
        </p>
      </div>
    </div>
  </div>

  <div class="container--section">
    <div class="pageContent">
      <?php echo get_the_content();?>
    </div>
  </div>

<?php endwhile; ?>

<?php get_footer(); ?>
