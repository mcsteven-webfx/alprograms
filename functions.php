<?php

/**
*
*  Functionalities of Alprograms Theme
*  Author: Mc Steven Dela
*  Version: 3.0
*  @package Alprograms
*  @since 2019
*
**/

/**
*
* Start Session
*
**/

function start_session() {
    if(!session_id()) {
        session_start();
    }
}

/**
*
* End Session
*
**/

function end_session() {
    session_destroy ();
}

add_action('init', 'start_session', 1);
add_action('wp_logout', 'end_session');
add_action('wp_login', 'end_session');

/**
*
* if alprograms_setup doesn't exist
*
**/

if ( ! function_exists( 'alprograms_setup' ) ) :
  /**
  *
  * Sets up theme defaults and registers support for various WordPress features
  *
  *  It is important to set up these functions before the init hook so that none of these
  *  features are lost.
  *
  **/

  function alprograms_setup() {
    /**
    *
    * Add Build in functionalities in wordpress dashboard
    *
    **/

    /* Post Formats */
    add_theme_support( 'post-formats' );
    /* Post Thumbnails */
    add_theme_support( 'post-thumbnails' );
    /* Custom Background */
    add_theme_support( 'custom-background' );
    /* Custom Logo */
    add_theme_support( 'custom-logo' );
    /* Automatic Feed Links */
    add_theme_support( 'automatic-feed-links' );
    /* Html5 Support */
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
    /* Title Tag */
    add_theme_support( 'title-tag' );
    /* Register Nav Menus */
    register_nav_menus( array(
    'header'   => __( 'Header Menu', 'alprograms_Header' ),
    'footer' => __( 'Footer Menu', 'alprograms_Footer' )
    ));
  }
endif;
add_action('after_setup_theme', 'alprograms_setup');

function alprograms_theme_scripts(){
  /**
  *
  * Add CSS and JS Scripts
  *
  **/

  /* CSS default file */
  wp_enqueue_style( 'style', get_stylesheet_uri());
  /* Global CSS */
  wp_enqueue_style( 'global', get_template_directory_uri() . '/assets/css/global.css');
  /* TopPage CSS */
  wp_enqueue_style( 'homepage', get_template_directory_uri() . '/assets/css/home.css');
  /* Category CSS */
  wp_enqueue_style( 'category', get_template_directory_uri() . '/assets/css/category.css');
  /* Page CSS */
  wp_enqueue_style( 'page', get_template_directory_uri() . '/assets/css/page.css');
  /* Download PDF CSS */
  wp_enqueue_style( 'pdf', get_template_directory_uri() . '/assets/css/downloadPDF.css');
  /* Quotation CSS */
  wp_enqueue_style( 'quotation', get_template_directory_uri() . '/assets/css/quotation.css');
  /* Single CSS */
  wp_enqueue_style( 'single', get_template_directory_uri() . '/assets/css/single.css');
  /* Swiper JS */
  wp_enqueue_script( 'swiper', get_template_directory_uri() . '/assets/js/swiper.js', array ( 'jquery' ));
  /* Navigation */
  wp_enqueue_script( 'navigation', get_template_directory_uri() . '/assets/js/navigation.js', array ( 'jquery' ));
  /* Brochure */
  wp_enqueue_script( 'brochure', get_template_directory_uri() . '/assets/js/brochure02.js', array ( 'jquery' ));
  /*
  *
  * External Links
  *
  */

  /* Font */
  wp_enqueue_style( 'Font', 'https://fonts.googleapis.com/css?family=Roboto&display=swap');
  /* Icons */
  wp_enqueue_script( 'icons', 'https://kit.fontawesome.com/41473e2a7f.js');
  /* Swiper CSS */
  wp_enqueue_style( 'swiper-css', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.css');
  /* Swiper CSS Minified*/
  wp_enqueue_style( 'swiper-css-min', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css');
  /* swiper JS */
  wp_enqueue_script( 'swiper-js', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.js');
  /* swiper JS Minified*/
  wp_enqueue_script( 'swiper-js-min', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js');
  /* recaptcha */
  wp_enqueue_script( 'recaptcha', 'https://www.google.com/recaptcha/api.js');
}
add_action( 'wp_enqueue_scripts', 'alprograms_theme_scripts' );

/**
*
* Custom Fields Array
* - Sessions
* - Lectures
* - Level
*
**/

$prefix = 'course_';
$custom_meta_fields = array(
	array(
	    'label' => 'Sessions/Days',
	    'desc'  => 'Number of days spinner',
	    'id'    => $prefix.'days',
	    'type'  => 'number',
	    'min'   => '0',
	),
	array(
		'label' => 'Lectures',
		'desc'  => 'Number of lectures spinner',
		'id'    => $prefix.'lectures',
		'type'  => 'number',
		'min'   => '0',
	),
	array(
		'label' => 'Level',
		'desc'	=> 'Course difficulty',
		'id'	=> $prefix.'level',
		'type'	=> 'radio',
		'options' => array (

			'Beginner' => array (
				'label' => 'Beginner',
				'value' => 'Beginner'
			),
			'Intermediate' => array (
				'label' => 'Intermediate',
				'value' => 'Intermediate'
			),
			'Advance' => array (
				'label' => 'Advance',
				'value' => 'Advance'
			),
			'Expert' => array (
				'label' => 'Expert',
				'value' => 'Expert'
			)
		)
	),
  array(
	    'label' => 'Price',
	    'desc'  => 'Price of the course',
	    'id'    => $prefix.'price',
	    'type'  => 'number',
	    'min'   => '0',
	),
  array(
      'label' => 'Max',
      'desc'  => 'maximum people per course',
      'id'    => $prefix.'max',
      'type'  => 'number',
      'min'   => '0',
  ),
  array(
      'label' => 'Per Head',
      'desc'  => 'Per Head price',
      'id'    => $prefix.'head',
      'type'  => 'number',
      'min'   => '0',
  ),
  array(
      'label' => 'Online Price',
      'desc'  => 'Price of the course',
      'id'    => $prefix.'onlineprice',
      'type'  => 'number',
      'min'   => '0',
  ),
  array(
      'label' => 'Online Max',
      'desc'  => 'maximum people per course',
      'id'    => $prefix.'onlinemax',
      'type'  => 'number',
      'min'   => '0',
  ),
  array(
      'label' => 'Online Per Head',
      'desc'  => 'Per Head price',
      'id'    => $prefix.'onlinehead',
      'type'  => 'number',
      'min'   => '0',
  )
);

/**
*
* Register Custom Meta Boxes
*
**/

function register_custom_meta_box(){
  //OUTLINE EDITOR
  add_meta_box( 'post_outline', __('Outline', 'wysiwyg'), 'course_outline', 'post');
  //MODULE EDITOR
  add_meta_box( 'post_module', __('Module', 'wysiwyg'), 'course_module', 'post');
  //CUSTOM FIELD
  add_meta_box( 'post', 'custom_fields', 'show_course_custom_fields', 'post');
}add_action( 'admin_init', 'register_custom_meta_box');

/**
*
* Function for outline editor
*
**/

function course_outline($post) {
	$content = get_post_meta($post->ID, 'course_outline_wysiwyg', true);
	wp_editor(htmlspecialchars_decode($content) , 'course_outline_wysiwyg', array("media_buttons" => true));
}

/**
*
* Function for Module editor
*
**/

function course_module($post) {
	$content = get_post_meta($post->ID, 'course_module_wysiwyg', true);
	wp_editor(htmlspecialchars_decode($content) , 'course_module_wysiwyg', array("media_buttons" => true));
}

/**
*
* Save data for Outling and Module editor
*
**/

function custom_wysiwyg_save_postdata($post_id) {
	global $custom_meta_fields;

	// verify nonce
	if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}

  /**
  *
  * Updating Outline and Module editor
  *
  **/

	$data = $_POST['course_outline_wysiwyg'];
	update_post_meta($post_id, 'course_outline_wysiwyg', $data);
	$data = $_POST['course_module_wysiwyg'];
	update_post_meta($post_id, 'course_module_wysiwyg', $data);

 }
add_action('save_post', 'custom_wysiwyg_save_postdata');

/**
*
* Callback custom fields
*
**/

function show_course_custom_fields() {
	global $custom_meta_fields, $post;
	// Use nonce for verification
	echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	    // Begin the field table and loop
	    echo '<table class="form-table">';
	    foreach ($custom_meta_fields as $field) {
	        // get value of this field if it exists for this post
	        $meta = get_post_meta($post->ID, $field['id'], true);
	        // begin a table row with
	        echo '<tr>
	                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
	                <td>';
	                switch($field['type']) {
						case 'number':
							$value = $meta != '' ? $meta : $field['min'];
							    echo '<div id="'.$field['id'].'"></div>
							            <input type="'.$field['type'].'" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$value.'" min="'.$field['min'].'"/>
							            <br /><span class="description">'.$field['desc'].'</span>';
							break;
						case 'radio':
							foreach( $field['options'] as $option ) :
								echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'"'. ($meta == $option['value'] ? ' checked="checked"' : '') .'/>
									  <label for="'.$option['value'].'">'.$option['label'].'</label><br/>';
							endforeach;
							break;
	                } //end switch
	        echo '</td></tr>';
	    } // end foreach
	    echo '</table>'; // end table
}

/**
*
* Save Custom Fields
*
**/

function save_custom_meta_data($post_id) {
	// loop through fields and save the data
  global $custom_meta_fields;

	foreach ($custom_meta_fields as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif (' ' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // end foreach
}
add_action( 'save_post', 'save_custom_meta_data');

/**
*
* Add Post Columns
*
**/

function add_post_columns($columns){
  global $custom_meta_fields;
	foreach($custom_meta_fields as $field) {
		$columns[$field['id']] = __($field['label'], 'ALPrograms');
	}
    return $columns;
}
add_filter( 'manage_post_posts_columns', 'add_post_columns');

/**
*
* Post Custom Columns
*
**/

function custom_columns( $column, $post_id ) {
	echo get_post_meta($post_id, $column, true);
}
add_action('manage_post_posts_custom_column' , 'custom_columns', 10, 2 );

/**
*
* Sortable Custom Columns
*
**/

function add_post_columns_sortable($columns) {
	global $custom_meta_fields;
	foreach($custom_meta_fields as $field) {
		$columns[$field['id']] = $field['id'];
	}
  $columns['categories'] = 'categories';
  return $columns;
}
add_filter('manage_edit-post_sortable_columns' , 'add_post_columns_sortable');

/**
*
* Order Custom Columns
*
**/

function manage_wp_posts_pre_get_posts( $query ) {

	$orderby = $query->get('orderby');

	if($orderby == 'course_lectures' || $orderby == 'course_days') {
		$query->set( 'meta_key', $orderby);
		$query->set( 'orderby', 'meta_value_num' );
	} else if($orderby == 'course_level' || $orderby == 'course_featured') {
		$query->set( 'meta_key', $orderby);
		$query->set( 'orderby', 'meta_value' );
	}

}
add_action( 'pre_get_posts', 'manage_wp_posts_pre_get_posts');

/**
*
* Search by title only
*
**/

function search_by_title_only( $search, $wp_query )
{
    global $wpdb;
    if(empty($search)) {
        return $search; // skip processing - no search term in query
    }
    $q = $wp_query->query_vars;
    $n = !empty($q['exact']) ? '' : '%';
    $search =
    $searchand = '';
    foreach ((array)$q['search_terms'] as $term) {
        $term = esc_sql($wpdb->esc_like($term));
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }
    if (!empty($search)) {
        $search = " AND ({$search}) ";
        if (!is_user_logged_in())
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }
    return $search;
}
add_filter('posts_search', 'search_by_title_only', 500, 2);

function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

/**
*
* Add Custom Taxonomies
*
**/

function add_custom_types_to_tax( $query ) {
	if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

		// Get all your post types
		$post_types = get_post_types();

		$query->set( 'post_type', $post_types );
		return $query;
	}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );

/**
*
* Add Category
*
**/

function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
	register_taxonomy_for_object_type( 'category', 'page' );
}
add_action( 'init', 'add_taxonomies_to_pages' );
