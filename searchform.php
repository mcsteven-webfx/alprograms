<?php
/**
*
*  Search Form
*
*  @package Alprograms
*  @since 2019
*
**/
?>

<div class="container--section">
  <form role="search" method="get" class="search--form" action="<?php echo esc_url(home_url('/'));?>">
    <input type="text" placeholder="Search Courses here.." value="<?php echo get_search_query(); ?>" name="s" id="s" title="Search" autocomplete="off">
    <button type="submit">Search</button>
  </form>
</div>
