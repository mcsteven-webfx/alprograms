<?php

/**
*
*  Template Name: HomePage
*  @package Alprograms
*  @since 2019
*
**/

?>

<?php get_header(); ?>

<?php
if(isset($_POST['submit']) && !empty($_POST['submit'])):
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):

        //your site secret key
        $secret = '6Lc3YVkUAAAAAANbINejzYuOU6MfqzpEa6sbyU_H';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success):
            //contact form submission code
            $fullname = !empty($_POST['fullname'])?$_POST['fullname']:'';
            $email = !empty($_POST['email'])?$_POST['email']:'';
            $mobilenumber = !empty($_POST['mobilenumber'])?$_POST['mobilenumber']:'';

            $to = 'support@alprograms.com';
            $subject = 'New Contact form have been submitted';
            $htmlContent = "
                <h1>Contact request details</h1>
                <p><b>Name: </b>".$fullname."</p>
                <p><b>Email: </b>".$email."</p>
                <p><b>Mobilenumber: </b>".$mobilenumber."</p>
            ";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers

            //send email
            wp_mail($to,$subject,$htmlContent,$headers);

            $message = "request sent";
            echo "<script type='text/javascript'>alert('$message');</script>";
        else:
            $errMsg = 'Robot verification failed, please try again.';
        endif;
    else:
        $errMsg = 'Please click on the reCAPTCHA box.';
    endif;
else:
    $errMsg = '';
    $succMsg = '';
endif;
?>
<!-- Swiper -->
<div class="swiper-container section1--swiper">
  <div class="swiper-wrapper">
    <div class="swiper-slide" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/slidertop/Slider1.png') no-repeat center; background-size: cover;">
    </div>
    <div class="swiper-slide" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/slidertop/Slider2.png') no-repeat center; background-size: cover;">
    </div>
    <div class="swiper-slide" style="background: url('<?php echo get_template_directory_uri(); ?>/assets/img/slidertop/Slider3.png') no-repeat center; background-size: cover;">
    </div>
  </div>
  <div class="section1">
    <div class="section1--logo">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo-top.png" alt="logo top">
    </div>
    <div class="section1--content">
      <p class="section1__sentence1">
        Welcome to ALPs, your preferred training partner.
      </p>
      <p class="section1__sentence2">
        We aim to bring the best in-class training programs for you and your organization. We use interactive activities, up-to-date case studies and tailor fitted learning outcomes that will surely suit your training and development needs.
      </p>
      <p class="section1__sentence1">
        Explore our programs today.
      </p>
      <a href="https://www.alprograms.com/category/in-house-training/">Training Courses</a>
    </div>
  </div>
</div>

<div class="section2">
  <div class="container--section">
    <p class="section2__title section--title">
      What We Do
    </p>
    <div class="section2Content">
      <div class="section2--box">
        <div class="section2__image">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatwedo/box1.jpg" alt="box1">
        </div>
        <p class="section2__text">
          We help individuals achieve their full potential in the workplace and in their daily lives through up-to-date training materials and techniques.
        </p>
      </div>
      <div class="section2--box">
        <div class="section2__image">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatwedo/box2.jpg" alt="box2">
        </div>
        <p class="section2__text">
          We offer training programs that are created by professionals for professionals.
      </div>
      <div class="section2--box">
        <div class="section2__image">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatwedo/box3.jpg" alt="box3">
        </div>
        <p class="section2__text">
          We use case samples that are relatable and useful for everyday activities in the work place and daily lives.
        </p>
      </div>
      <div class="section2--box">
        <div class="section2__image">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatwedo/box4.jpg" alt="box4">
        </div>
        <p class="section2__text">
          We provide a learning environment that is engaging, interactive and fun.
        </p>
      </div>
      <div class="section2--box">
        <div class="section2__image">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatwedo/box5.jpg" alt="box5">
        </div>
        <p class="section2__text">
          We follow through with our partners to ensure that benefits of the training programs are achieved - we take feedback seriously.
        </p>
      </div>
    </div>
  </div>
</div>

<div class="section3">
  <div class="container--section">
    <p class="section3__title section--title">
      How we do it
    </p>
    <div class="section3Content">
      <div class="section3--box">
        <p class="section3__text">
          A TAILORED TRAINING EXPERIENCE DESIGNED FOR OPTIMUM LEARNING ABSORPTION
          <br><br>
          When you take people off their day job to learn, it’s vital that they receive training that is practical and relevant to them.
          <br><br>
          Our sessions have a balance of 20% theory, with 80% activities. Just as importantly, we’ll ensure the experience is tailored to your workplace and their roles.
          <br><br>
          We always ensure the learning is built around their roles and your goals.
        </p>
      </div>
      <div class="section3__line"></div>
      <div class="section3--box">
        <p class="section3__text">
          WOULD YOU PREFER TAILORED OR CUSTOMISED?
          <br><br>
          Tailored is our standard: We adapt the terminology, activities and positioning of the day to be relevant to your needs. This is our standard approach and is built into your quoted price. It happens as a simple conversational process during the pre-delivery period.
          <br><br>
          Customised (optional): Sometimes, a course needs to be written specifically for your needs. Our instructional designers can create material built specifically around your needs. We can usually minimise development time by leveraging our extensive curriculum, so we avoid the time and cost of recreating the wheel.
          <br><br>
          When we are creating courses for custom system roll outs or organisational re-design, we can work with you from process mapping through to complete custom creation, hand-over, train the trainer or training delivery.
        </p>
      </div>
    </div>
  </div>
</div>

<div class="section5">
  <div class="container--section">
    <p class="section5__title section--title">We are glad to share some great feedback from our training partners</p>

    <div class="swiper-container section5--swiper">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div class="section5--img">
            <img src="https://www.alprograms.com/wp-content/uploads/2021/08/maxi.png" style="width:60%" alt="">
          </div>
          <p class="section5__text">
            "It was an absolute pleasure meeting and being inspired by the trainer. I have already recommended this course to other staff and told them to request him as the trainer. Thanks." - Maxicare
          </p>
        </div>
        <div class="swiper-slide">
          <div class="section5--img">
            <img src="https://www.alprograms.com/wp-content/uploads/2021/08/maersk.png" style="width:60%" alt="">
          </div>
          <p class="section5__text">
            "I would just like to say that the course was a lot of fun and certainly gave insight to things I had not been aware of before even though I have spent many years in Customer Service. Thanks for a great experience" - Maersk Drilling
          </p>
        </div>
        <div class="swiper-slide">
          <div class="section5--img">
            <img src="https://www.alprograms.com/wp-content/uploads/2021/08/crimson.png" style="width:50%" alt="">
          </div>
          <p class="section5__text">
            "This was one of the most productive training sessions that I have done in the last 5 years. Good content delivered very well. I felt the trainer as very informative and knowledgeable and delivered the session well, I enjoyed the session and gained some well needed knowledge". - Crimson Hotel Mactan
          </p>
        </div>
        <div class="swiper-slide">
          <div class="section5--img">
            <img src="https://www.alprograms.com/wp-content/uploads/2021/08/carousel.png" style="width:60%" alt="">
          </div>
          <p class="section5__text">
            "This is the best course I have ever done"  - Carousell
          </p>
        </div>
        <div class="swiper-slide">
          <div class="section5--img">
            <img src="https://www.alprograms.com/wp-content/uploads/2021/08/bigcat.png" style="width:65%" alt="">
          </div>
          <p class="section5__text">
            "Highly targeted (and therefore valuable) training given that there were only two delegates - Josh managed superbly to balance our individual requirements." - Big Cat Solutions
          </p>
        </div>
      </div>
      <div class="swiper-pagination"></div>
    </div>

  </div>
</div>

<div class="section4">
  <div class="container--section">
    <p class="section4__title section--title">Training Articles</p>

    <div class="swiper-container section4--swiper">
      <div class="swiper-wrapper">
        <?php
          $post_type = array(
          'post_type' => 'post',
          'post_status' => 'publish',
          'cat' => 581,
          'posts_per_page' => -1,
          'order' => 'asc',
          'orderby' => 'title'
          );
        ?>
        <?php

        $course_query = new WP_query($post_type);

        if ( $course_query->have_posts() ) :
          while ( $course_query->have_posts() ) : $course_query->the_post();

          $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
          $bg = 'background: url('.$url.') no-repeat center; background-size: cover;';
        ?>
          <div class="swiper-slide">
            <a href="<?php echo get_permalink(); ?>" class="section4--box">
              <div class="section4--image" style="<?php echo $bg; ?>">

              </div>
              <p class="section4Box__title"><?php echo get_the_title(); ?> </p>
            </a>
          </div>
        <?php
          endwhile;
        endif;
        ?>
      </div>
    </div>

  </div>
</div>

<div class="section6">
  <div class="container--section">
    <p class="section6__title section--title">Our Training Partners</p>

    <div class="swiper-container section6--swiper">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
        <img src="https://www.alprograms.com/wp-content/uploads/2021/08/cola.png" style="width: 90%;" alt="Coca Cola">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/ovp.png" style="width: 90%;" alt="Office of the Vice President">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/sanmig.png" style="width: 90%;" alt="San Miguel">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/sqreem.png" style="width: 100%;" alt="Sqreem">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/solid.png" style="width: 100%;" alt="Solid.ph">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/toyota.png" style="width: 100%;" alt="Toyota">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/maxi.png" style="width: 100%;" alt="Maxicare">
        </div>
        <div class="swiper-slide">
          <img src="https://www.alprograms.com/wp-content/uploads/2021/08/dpwh.png" style="width: 90%;" alt="Department of Public Works and Highway">        
		</div>
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>
</div>

<div class="section7">
  <div class="container--section">
    <p class="section7__title section--title">Contact Us</p>
    <div class="section7Content">
      <form name="Contact" method="post" action="" class="section7--form">
        <input type="text" name="fullname" placeholder="Full Name" required><br>
        <input type="text" name="email" placeholder="E-mail" required><br>
        <input type="number" name="mobilenumber" placeholder="Mobile Number" required><br>
        <div class="recaptcha">
          <div class="g-recaptcha" data-sitekey="6Lc3YVkUAAAAAF3ir5Z9pqjb1DnMEOqclwWKhmzd" data-callback="recaptchaCallback"></div>
        </div><br>
        <input type="submit" name="submit" onclick="sendtoSpreedSheets()" value="Submit">
      </form>
      <div class="section7--line"></div>
      <div class="section7--text">
        <p class="section7__text1">Talk to us about <br><span>Your Needs</span> now.</p>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>

<script>
  function sendtoSpreedSheets(){
    const scriptURL = 'https://script.google.com/macros/s/AKfycbzU7VSH1p9vvUHrJ7Pi0lLW7RfWWbIGIIFaJygGhbzNbCyHl74/exec'
    const form = document.forms['Contact']
    fetch(scriptURL, { method: 'POST', body: new FormData(form)})
      .then(response => console.log('Success!', response))
      .catch(error => console.error('Error!', error.message))
  }
</script>
