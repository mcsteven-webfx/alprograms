<?php
/*
 *
 * Template Name: Quotation
 *
*/
?>
<script src="http://code.jquery.com/jquery-1.9.0rc1.js"></script>

<?php
if(isset($_POST['submit']) && !empty($_POST['submit'])):
  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):

    //your site secret key
    $secret = '6Lc3YVkUAAAAAANbINejzYuOU6MfqzpEa6sbyU_H';
    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success):

      $fullname = !empty($_POST['fullname'])?$_POST['fullname']:'';
      $NoP = !empty($_POST['nop'])?$_POST['nop']:'';
      $courses = !empty($_POST['courses'])?$_POST['courses']:'';
      $mobilenumber = !empty($_POST['mobilenumber'])?$_POST['mobilenumber']:'';
      $email = !empty($_POST['email'])?$_POST['email']:'';
      $type = !empty($_POST['training'])?$_POST['training']:'';

      $to = 'support@alprograms.com';
      $subject = 'New Contact form have been submitted';
      $htmlContent = "
          <h1>Quotation request details</h1>
          <p><b>Name: </b>".$fullname."</p>
          <p><b>Email: </b>".$email."</p>
          <p><b>Mobilenumber: </b>".$mobilenumber."</p>
          <p><b>Course: </b>".$courses."</p>
          <p><b>Type of Training: </b>".$type."</p>
      ";
      // Always set content-type when sending HTML email
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


      //send email
      wp_mail($to,$subject,$htmlContent,$headers);

      global $post;
      $course_id;
      $args = array( 'numberposts' => -1);
      $posts = get_posts($args);
      foreach( $posts as $post ) :
        if($courses == get_the_title()){
          $Nop1 = (int)$NoP;
          $course_id = get_the_ID();

          if(strcmp($type, 'inhouse') == 0):
            $PerH = get_post_meta(get_the_ID(), 'course_head', true);
            $_SESSION['setup'] = 'inhouse';
          elseif(strcmp($type, 'online') == 0):
            $PerH = get_post_meta(get_the_ID(), 'course_onlinehead', true);
            $_SESSION['setup'] = 'online';
          endif;
          $Total = $Nop1 * $PerH;
          $Vat = $Total * 0.12;
          $TotalVAT = ($Total * 0.12)+$Total;
          setup_postdata($post);
        }
      endforeach;

      $_SESSION['fullname'] = $fullname;
      $_SESSION['courses'] = $courses;
      $_SESSION['location'] = $location;
      $_SESSION['totalvat'] = $TotalVAT;
      $_SESSION['total'] = $Total;
      $_SESSION['nop'] = $NoP;
      $_SESSION['course_id'] = $course_id;
      $_SESSION['display'] = 1;

      $url = 'https://www.alprograms.com/download-pdf';
      echo("<script>location.href = '".$url."'</script>");

    else:
      $errMsg = 'Robot Verification failed, Please try again.';
    endif;
  else:
    $errMsg = 'Please click on the reCAPTCHA box.';
  endif;
endif;
?>
<?php get_header(); ?>
<?php
  $url = wp_get_attachment_url( get_post_thumbnail_id( $post -> ID ));
  $bg = 'background: url('.$url.') no-repeat center; background-size: cover; opacity: 0.7;';
?>
<div class="QuotationandContact-Container-Header">
  <div class="QuotationandContact-Image" style="<?php echo $bg; ?>">

  </div>
  <div class="QuotationandContact-Title">
    <?php the_title(); ?>
  </div>
  <div class="QuotationandContact-Description">
    <?php echo get_the_excerpt(); ?>
  </div>
</div>

<div class="QuotationandContact-Content">
  <form name="Quotation" id="formRequest" method="POST" action="">
    <p class="QuotationandContact-Content-Text">Full Name:</p>
    <input type="text" name="fullname" placeholder="Full Name" required>
    <p class="QuotationandContact-Content-Text">Email:</p>
    <input type="text" name="email" placeholder="Email" required>
    <p class="QuotationandContact-Content-Text">Address:</p>
    <input type="text" name="address" placeholder="Address" required>
    <p class="QuotationandContact-Content-Text">Mobile Number:</p>
    <input type="text" name="mobilenumber" placeholder="Mobile Number" required>
    <p class="QuotationandContact-Content-Text">Number of Participants</p>
    <input type="number" name="nop" placeholder="Max Number of 40pax" min="1" max="40" required>
    <?php
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'category_name' => 'in-house-training',
        'post_per_page' => -1,
        'order' => 'asc',
        'orderby' => 'title',
        'numberposts' => -1
      );
    ?>
    <p class="QuotationandContact-Content-Text">Courses:</p>
    <select name="courses" id="courses" value="<? echo $ID2; ?>">
    <?php
    global $post;
    $posts = get_posts($args);
     foreach($posts as $post):
       $ID2 = $post->ID;
       setup_postdata($post);
    ?>
    <option name ="courses" id="courses" ><?php the_title(); ?></option>
    <?php endforeach; ?>
    </select>
    <p class="QuotationandContact-Content-Text">Type of Training:</p>
    <select name="training" id="type">
      <option name ="inhouse" id="inhouse" value="inhouse">In-house Training</option>
      <option name ="online" id="online" value="online">Online Training</option>
    </select>

    <div class="recaptcha">
        <br>
        <div class="g-recaptcha" data-sitekey="6Lc3YVkUAAAAAF3ir5Z9pqjb1DnMEOqclwWKhmzd" data-callback="recaptchaCallback"></div>
    </div>
    <div class="QuotationandContact-submit">
      <input type="submit" name="submit" onclick="sendtoSpreedSheets()"  value="submit"/>
    </div>
  </form>
</div>
  <script>
    function sendtoSpreedSheets(){
      const scriptURL = 'https://script.google.com/macros/s/AKfycby8NegQPeRZAyr_q_uJUQJD0qlQZZfgcYJPW_jn3iBY8f7E6PI/exec'
      const form = document.forms['Quotation']
      fetch(scriptURL, { method: 'POST', body: new FormData(form)})
        .then(response => console.log('Success!', response))
        .catch(error => console.error('Error!', error.message))
    }

    function recaptchaCallback() {
      $('#submit').removeAttr('disabled');
    };
  </script>
<?php get_footer();?>
