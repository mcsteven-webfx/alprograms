<?php
/*
 *
 * Template Name: DownloadPDF
 *
*/
?>

<?php get_header(); ?>

<?php session_start(); ?>

<?php
  $display = $_SESSION['display'];
  $course_id = $_SESSION['course_id'];
  $setup = $_SESSION['setup'];
  $brochure = get_field('file', $course_id);
  $url = wp_get_attachment_url( get_post_thumbnail_id( $post -> ID ));
  $bg = 'background: url('.$url.') no-repeat center; background-size: cover; opacity: 0.7;';
?>
<div class="downloadpdf-container">
  <div class="downloadpdf-img" style="<?php echo $bg; ?>">

  </div>
  <div class="downloadpdf-button-wrapper">
    <div class="downloadpdf-button-center">
      <?php if($display == 1){?>
          <?php if(!empty($brochure)): ?>
                <?php if(strcmp($setup, 'inhouse') == 0): ?>
                    <a class="downloadpdf-button" href="<?php echo(get_template_directory_uri() . '/inc/print_page_qoutation.php');?>" data-link="<?php echo $brochure['url']; ?>">Download Quotation <i class="fa fa-file-pdf-o"></i></a>
                <?php elseif(strcmp($setup, 'online') == 0): ?>
                    <a class="downloadpdf-button" href="<?php echo(get_template_directory_uri() . '/inc/print_page_qoutation_online.php');?>" data-link="<?php echo $brochure['url']; ?>">Download Quotation <i class="fa fa-file-pdf-o"></i></a>
                <?php endif; ?>
          <?php else: ?>
              <?php if(strcmp($setup, 'inhouse') == 0): ?>
                  <a class="downloadpdf-button" href="<?php echo(get_template_directory_uri() . '/inc/print_page_qoutation.php');?>" data-link="<?php echo(get_template_directory_uri() . '/inc/print_page.php'); ?>">Download Quotation <i class="fa fa-file-pdf-o"></i></a>
              <?php elseif(strcmp($setup, 'online') == 0): ?>
                  <a class="downloadpdf-button" href="<?php echo(get_template_directory_uri() . '/inc/print_page_qoutation_online.php');?>" data-link="<?php echo(get_template_directory_uri() . '/inc/print_page.php'); ?>">Download Quotation <i class="fa fa-file-pdf-o"></i></a>
              <?php endif; ?>
          <?php endif; ?>
      <?php } else if($display == 2){ ?>
        <?php if(!empty($brochure)): ?>
          <a class="downloadpdf-button" href="<?php echo $brochure['url'];?>">Download PDF <i class="fa fa-file-pdf-o"></i></a>
        <?php else: ?>
          <a class="downloadpdf-button" href="<?php echo(get_template_directory_uri() . '/inc/print_page.php');?>">Download PDF <i class="fa fa-file-pdf-o"></i></a>
        <?php endif; ?>
      <?php }  ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
