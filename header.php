<?php

/**
*
*  Header Navigation
*  @package Alprograms
*  @since 2019
*
**/

?>
<!Doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?php bloginfo( 'charset' );?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137241647-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-137241647-1');
    </script>
    <?php wp_head(); ?>
  </head>
  <body>
    <div class="container--main">
      <header>
        <div class="header--section">
          <div class="header--wrapper">
            <h1 class="logoContainer">
              <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="logo"></a>
            </h1>
            <button class="mobile--nav"><i class='fas fa-bars'></i></button>
          </div>
          <div class="header--navigation">
            <nav class="headerNav container--flex">
              <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            </nav>
            <a href="https://www.alprograms.com/quotation/" class="header--button">Request Quotation</a>
            <div class="headerContacts">
              <p class="header__number">(02) 7-902-0992  / 0917 6247037</p>
              <a href="mailto:support@alprograms.com" class="header__email">support@alprograms.com</a>
            </div>
          </div>
        </div>
      </header>
