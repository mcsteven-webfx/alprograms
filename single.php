<?php

/**
*
*  Single Post
*
*  @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*  @package Alprograms
*  @since 2019
*
**/

?>

<?php
if(isset($_POST['submit']) && !empty($_POST['submit'])):
  if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):

    //your site secret key
    $secret = '6Lc3YVkUAAAAAANbINejzYuOU6MfqzpEa6sbyU_H';
    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success):

      $fullname = !empty($_POST['fullname'])?$_POST['fullname']:'';
      $courses = get_the_title();
      $email = !empty($_POST['email'])?$_POST['email']:'';

      $to = 'support@alprograms.com';
      $subject = 'New Contact form have been submitted';
      $htmlContent = "
          <h1>DownloadPDF request details</h1>
          <p><b>Name: </b>".$fullname."</p>
          <p><b>Email: </b>".$email."</p>
          <p><b>Course: </b>".$courses."</p>
      ";
      // Always set content-type when sending HTML email
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


      //send email
      wp_mail($to,$subject,$htmlContent,$headers);

      $url = 'https://www.alprograms.com/download-pdf';
      echo("<script>location.href = '".$url."'</script>");

    else:
      $errMsg = 'Robot Verification failed, Please try again.';
    endif;
  else:
    $errMsg = 'Please click on the reCAPTCHA box.';
  endif;
endif;
?>

<?php get_header(); ?>
  <?php while(have_posts()): the_post(); ?>

    <?php
      $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
      $bg = 'background: url('.$url.') no-repeat center; background-size: cover;';
    ?>
    <div class="singleContainer" style="<?php echo $bg; ?>">
      <?php $hide = get_field('options'); ?>
      <?php if ( empty( $hide )): ?>
        <div class="container--section">
          <div class="single--title">
            <p class="single__title">
              <?php the_title(); ?>
            </p>
          </div>
        </div>
      <?php endif; ?>
    </div>

    <?php get_search_form(); ?>

    <div class="container--section">
      <div class="single--content">
        <?php if(in_category(8) || in_category(1094)){ ?>
          <h2>Description</h2><br>
          <?php the_content(); ?>
          <br><h2>Course Objective</h2><br>
          <?php $course_outline = get_post_meta( get_the_id(), 'course_outline_wysiwyg', true );
                echo $course_outline;
          ?>
          <br><h2>Modules</h2><br>
          <?php
            $course_module = get_post_meta(get_the_ID(), 'course_module_wysiwyg', true);
            echo $course_module;
          ?>

          <div class="single--card">
            <ul>
              <li><button id="inHouseCard">In-House Training</button></li>
              <li><button id="publicCard">Public Courses</button></li>
            </ul>
            <div class="inhouse--card">
              <div class="inhouse--card__box1">
                <p>
                  - Flexible length - sessions as short as 1-hour <br>
                  - Cost effective - great group pricing <br>
                  - Greatest impact in the shortest time <br>
                  - Excellent Team Building Opportunity <br>
                  - Certificate of Completion <br>
                </p>
                <a href="https://www.alprograms.com/quotation/">Request Quotation</a>
              </div>
              <div class="inhouse--card__box2">
                <img src="https://www.alprograms.com/wp-content/uploads/2017/11/classroom-2093745_1920.jpg" alt="">
              </div>
            </div>
            <div class="public--card">
              <iframe src="https://calendar.google.com/calendar/b/2/embed?height=400&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Asia%2FManila&amp;src=YWxwcm9ncmFtcy5jb21fOHEwamRhOWh1aWI3Y2djNTg5ZmVidDYwZjhAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%23EF6C00&amp;showTitle=0&amp;showNav=0&amp;showTz=0&amp;showCalendars=0&amp;showPrint=0&amp;showTabs=0&amp;mode=MONTH&amp;title=Public%20Courses%20Schedule" style="border-width:0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>
            </div>
          </div>

        <?php }else{
          the_content();
        } ?>
      </div>
    </div>

    <?php if(in_category(8) || in_category(1094)){ ?>
      <div class="singlePDF" style="background: url('https://www.alprograms.com/wp-content/uploads/2019/10/Page2.png') no-repeat center; background-size: cover;">
        <div class="container--section">
          <div class="singlePDF--content">
            <p class="singlePDF__text">
              Download the course PDF now!
            </p>

            <?php
              $_SESSION['course_title'] = get_the_title();
              $_SESSION['course_description'] = get_the_content();
              $_SESSION['course_outline'] = $course_outline;
              $_SESSION['course_module'] = $course_module;
              $_SESSION['course_id'] = get_the_ID();
              $_SESSION['display'] = 2;
            ?>

            <form name="Download" method="POST" action="">
              <p>Fullname</p>
              <input type="text" name="fullname" placeholder="Full Name" required>
              <p>E-mail</p>
              <input type="text" name="email" placeholder="Email Address" required>
              <p>Course</p>
              <input type="text" class="Course-Input" name="course" value="<?php echo get_the_title() ?>" disabled>
              <div class="recaptcha">
                  <div class="g-recaptcha" data-sitekey="6Lc3YVkUAAAAAF3ir5Z9pqjb1DnMEOqclwWKhmzd" data-callback="recaptchaCallback"></div>
              </div>
              <input type="submit" name="submit" onclick="sendtoSpreedSheets()" value="Download PDF"/>
            </form>
          </div>
        </div>
      </div>
    <?php } ?>
  <?php endwhile; ?>

<?php get_footer(); ?>
<script>
  function sendtoSpreedSheets(){
    const scriptURL = 'https://script.google.com/macros/s/AKfycbzsg7vYXuMtSKJorbudSSytEMiTFpMUsIr_n1VsIZk0dvMTgidg/exec'
    const form = document.forms['Download']
    fetch(scriptURL, { method: 'POST', body: new FormData(form)})
      .then(response => console.log('Success!', response))
      .catch(error => console.error('Error!', error.message))
  }
</script>
